"""Else - Finally"""

try:
    N1 = int(input("Ingresa primer número: "))
except Exception as e:
    print("Error")
else:
    print("Sin errores")
finally:
    print("Se ejecuta siempre!!!")
