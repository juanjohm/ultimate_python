"""Listas"""

NUMEROS = [1, 2, 3]
LETRAS = ["a", "b", "c"]
PALABRAS = ["JUAN", "PEDRO"]
BOOLEANS = [True, False, True, True]
MATRIZ = [[0, 1], [1, 0]]
CEROS = [0] * 10
ALFANUMERICO = NUMEROS + LETRAS
RANGO = list(range(1, 11))
CHARS = list("Hola Mundo")
print(CHARS)
