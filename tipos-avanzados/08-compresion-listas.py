"""Compresión listas"""

USUARIOS = [
    ["PEPE", 1], 
    ["LIRA", 4], 
    ["JUAN", 3], 
    ["LARA", 2]
]

# NOMBRES = [usuario[0] for usuario in USUARIOS]
# print(NOMBRES)

# #filtar
# NOMBRES = [usuario for usuario in USUARIOS if usuario[1] > 2]
# print(NOMBRES)

# #filtar y tranformada
# NOMBRES = [usuario[0] for usuario in USUARIOS if usuario[1] > 2]
# print(NOMBRES)

#Map
# NOMBRES = list(map(lambda usuario: usuario[0], USUARIOS))
# print(NOMBRES)

#Filter
MENOS_NOMBRES = list(filter(lambda usuario: usuario[1] > 2, USUARIOS))
print(MENOS_NOMBRES)
