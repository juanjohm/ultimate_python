"""Buscar elemento"""

MASCOTAS = ["perro", "gato", "tortuga", "pajaro", "gato"]

print(MASCOTAS.index("gato"))
print(MASCOTAS.count("gato"))
if "gato" in MASCOTAS:
    print(MASCOTAS.index("gato"))
