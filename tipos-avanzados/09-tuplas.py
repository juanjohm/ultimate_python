"""Tuplas"""
#Las tuplas se usan cuando no queramos modificar listados de manera accidental, NO PODEMOS ELIMINAR
NUMEROS = (1, 2, 3,) + (4, 5, 6)

print(NUMEROS)

PUNTO = tuple ([1, 2])
print(PUNTO)
MENOS_NUMEROS = NUMEROS[:2]
print(MENOS_NUMEROS)

PRIMERO, SEGUNDO, *OTROS = NUMEROS
print(PRIMERO, SEGUNDO, OTROS)
