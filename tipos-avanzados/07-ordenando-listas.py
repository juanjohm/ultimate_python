"""Ordenando listas"""

NUMEROS = [1, 52, 98, 63, 44, 34, 21]

#ordena lista
NUMEROS.sort()
print(NUMEROS)

#ordena lista de mayor  a menor
NUMEROS.sort(reverse=True)
print(NUMEROS)

#ordena creando nueva lista
NUMEROS2 = sorted(NUMEROS)
print(NUMEROS2)

#ordena lista de mayor  a menor
NUMEROS2 = sorted(NUMEROS, reverse=True)
print(NUMEROS2)

USUARIOS = [
    [4, "PEPE"], 
    [3, "LIRA"], 
    [2, "JUAN"], 
    [1, "LARA"]
]

USUARIOS.sort()
print(USUARIOS)

USUARIOS = [
    ["PEPE", 1], 
    ["LIRA", 4], 
    ["JUAN", 3], 
    ["LARA", 2]
]

def ordena(elemento):
    return elemento[1]

USUARIOS.sort(key=ordena)
print(USUARIOS)

USUARIOS.sort(key=ordena, reverse=True)
print(USUARIOS)

#manera corta, adecuada si no se va a usar más la funcion
USUARIOS.sort(key=lambda el: el[1], reverse=True)
print(USUARIOS)
