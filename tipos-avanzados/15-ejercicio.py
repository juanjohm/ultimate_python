from pprint import pprint
#Eliminar espacios en blanco
STRING = "Mi string es este"

def quitar_espacios(texto):
    return[char for char in texto if char != " "]

SIN_ESPACIOS = quitar_espacios(STRING)
print(SIN_ESPACIOS)


#Contar caracteres repetidos de string
def cuenta(lista):
    chars_dict = {}
    for char in lista:
        if char in chars_dict:
            chars_dict[char] += 1
        else:
            chars_dict[char] = 1
    return chars_dict
CONTADOS = cuenta(SIN_ESPACIOS)
pprint(CONTADOS, width=1)


#Ordenar llaves de diccionario por valor y devolver tuplas
def ordenar(dict):
    return sorted(
        dict.items(),
        key=lambda key: key[1],
        reverse=True,
    )
ORDENADOS = ordenar(CONTADOS)
print(ORDENADOS)

#Devolver el de mayor valor
def mayor(lista):
    MAX = lista[0][1]
    RESPUESTA = {}
    for orden in lista:
        if MAX > orden[1]:
            break
        RESPUESTA[orden[0]] = orden[1]
    return RESPUESTA

MAYORES = mayor(ORDENADOS)
print(MAYORES)


#Mostrar mensaje
def crea_mensaje(diccionario):
    MEN = "Los más repetidos son: \n"
    for key, valor in diccionario.items():
        MEN += f"- {key} con {valor} repeticiones \n"
    return MEN

MENSAJE = crea_mensaje(MAYORES)
print(MENSAJE)
