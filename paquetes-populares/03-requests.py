# import requests


# APIKEY = "123456"
# HEADERS = {
#     "Authorization": f"Bearer {APIKEY}"
# }


# URL = "https://jsonplaceholder.typicode.com/USERs"
# r = requests.get(URL, timeout=10, HEADERS=HEADERS)
# r = r.json()
# for USER in r:
#     print(USER["name"])


# URL = "https://jsonplaceholder.typicode.com/USERs/2"
# r = requests.get(URL, timeout=10, HEADERS=HEADERS)
# print(r.json())


# URL = "https://jsonplaceholder.typicode.com/USERs"
# USER = {
#     "id": 2,
#     "name": "Juan"
# }
# r = requests.post(URL, timeout=10, HEADERS=HEADERS, data=USER)
# print(r.status_code)


# # PUT y PATCH
# URL = "https://jsonplaceholder.typicode.com/USERs/2"
# USER = {
#     "id": 2,
#     "name": "Juan"
# }
# r = requests.put(URL, timeout=10, HEADERS=HEADERS, data=USER)
# print(r.status_code)