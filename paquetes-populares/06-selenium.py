# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# import os

# # Una vez terminada la creación del test, comentar estas líneas
# # options = webdriver.ChromeOptions()
# # options.add_experimental_option("detach", True)
# # BROWSER = webdriver.Chrome(options=options)

# BROWSER = webdriver.Chrome()
# BROWSER.implicitly_wait(10)
# BROWSER.get("https://githum.com")

# LINK = BROWSER.find_element(By.LINK_TEXT, "Sign in")
# LINK.click()

# USER_INPUT = BROWSER.find_element(By.ID, "login_field")
# PASS_INPUT = BROWSER.find_element(By.ID, "password")

# USER_INPUT.send_keys(os.environ.get("gh_user"))
# PASS_INPUT.send_keys(os.environ.get("gh_pass"))
# PASS_INPUT.send_keys(Keys.RETURN)

# PROFILE = BROWSER.find_element(
#     By.CLASS_NAME,
#     "css-truncate.css-truncate-target.ml-1"
# )
# LABEL = PROFILE.get_attribute("innerHTML")

# assert "Juan" in LABEL

# BROWSER.quit()
