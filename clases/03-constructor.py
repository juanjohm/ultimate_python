"""Constructor"""

class MiPerro:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def habla(self):
        print(f"{self.nombre} dice: Guauuauauau")


MIPERRO = MiPerro("Toto", 1)
MIPERRO2 = MiPerro("Laaaaa", 5)
print(MIPERRO.nombre)
print(MIPERRO2.nombre)
MIPERRO.habla()
MIPERRO2.habla()
