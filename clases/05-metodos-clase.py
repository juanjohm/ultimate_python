"""Metodos clases"""

class MiPerro:

    PATAS = 4

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    @classmethod
    def habla(cls):
        print("Guauuauauau")

    @classmethod
    def factory(cls):
        return cls("Tiiiiii", 4)


MiPerro.habla()
PERRO1 = MiPerro("TOTO", 2)
PERRO2 = MiPerro("LALALA", 8)
PERRO3 = MiPerro.factory()
print(PERRO3.edad, PERRO3.nombre)
