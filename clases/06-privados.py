"""Privados"""

class MiPerro:

    def __init__(self, nombre, edad):
        self.__nombre = nombre
        self.edad = edad

    def get_nombre(self):
        return self.__nombre
    
    def set_nombre(self, nombre):
        self.__nombre = nombre
    
    def habla(self):
        print(f"{self.__nombre} dice: Guauuauauau")

    @classmethod
    def factory(cls):
        return cls("Tiiiiii", 4)


PERRO1 = MiPerro.factory()
PERRO1.habla()
print(PERRO1.get_nombre())
print(PERRO1.__dict__)
print(PERRO1._MiPerro__nombre)
