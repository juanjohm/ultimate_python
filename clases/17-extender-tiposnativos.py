"""Extender Tipos Nativos"""

# LISTA = list([1, 2, 3, 4])

# LISTA.append(5)
# LISTA.insert(0, 0)

# print(LISTA)

class Lista(list):
    def prepend(self, item):
        self.insert(0, item)

LISTA = Lista([1, 2, 3, 4])
LISTA.append(5)
LISTA.prepend(0)

print(LISTA)
