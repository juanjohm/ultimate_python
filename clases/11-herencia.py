"""Herencia"""

class Animal:
    def comer(self):
        print("comiendo")

class Perro(Animal):
    def pasear(self):
        print("paseando")

class Toto(Perro):#Hereda de Perro y de Animal, se llama herencia multiple, no es buena practica
    def programar(self):
        print("programando")

PERRO = Perro()
TOTO = Toto()
PERRO.comer()
TOTO.comer()
