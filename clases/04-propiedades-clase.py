"""Propiedas clases"""

class MiPerro:

    PATAS = 4

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def habla(self):
        print(f"{self.nombre} dice: Guauuauauau")


MiPerro.PATAS = 3
MIPERRO = MiPerro("Toto", 1)
MIPERRO.PATAS = 2
MIPERRO2 = MiPerro("Laaaaa", 5)
print(MiPerro.PATAS)
print(MIPERRO.PATAS)
print(MIPERRO2.PATAS)
MIPERRO.habla()
MIPERRO2.habla()
