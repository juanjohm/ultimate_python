"""Clases Abstractas"""

from abc import ABC, abstractmethod

class Model(ABC):    
    @property
    @abstractmethod
    def tabla(self):
        pass 

    def guardar(self):
        print(f"Guardando {self.tabla} en BBDD")

    @classmethod
    def buscarId(self, _id):
        print(f"Buscando por ID {_id} en la tabla {self.tabla}")


class Usuario(Model):
    tabla = "Usuario"

USUARIO = Usuario()
USUARIO.guardar()
USUARIO.buscarId(1)
