"""Decorador Properties"""

class MiPerro:

    def __init__(self, nombre):
        self.nombre = nombre
    
    @property
    def nombre(self):
        print("Pasa por el getter")
        return self.__nombre
    @nombre.setter
    def nombre(self, nombre):
        print("Pasa por el setter")
        if nombre.strip():
            self.__nombre = nombre
        return


PERRO = MiPerro("TOTO")
print(PERRO.nombre)
