"""Creando tablas"""

import sqlite3

CON = sqlite3.connect("sqlite/app.db")
CURSOR = CON.cursor()
CURSOR.execute(
    """
    CREATE TABLE if not exists usuarios
    (id INTEGER primary key, nombre VARCHAR(50));
    """
)
CON.commit()
CON.close()
