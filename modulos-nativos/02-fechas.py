"""Fechas"""

# import time

# print(time.time())

from datetime import datetime

FECHA = datetime(2023, 1, 1)
FECHA2 = datetime(2023, 2, 1)
AHORA = datetime.now()
# print(FECHA)
# print(AHORA)

FECHA_STR = datetime.strptime("2023/01/03", "%Y/%m/%d")
print(FECHA_STR, FECHA2)

print(FECHA.strftime("%Y.%m.%d"))

print(FECHA > FECHA2)

print(
    FECHA.year,
    FECHA.month,
    FECHA.day,
    FECHA.hour,
    FECHA.min
)