"""Timedelta"""

from datetime import datetime, timedelta

FECHA1 = datetime(2023, 1, 1) + timedelta(weeks=1)
FECHA2 = datetime(2023, 2, 1)

DELTA = FECHA2 - FECHA1

print(DELTA)
print("días", DELTA.days)
print("sec", DELTA.seconds)
print("micros", DELTA.microseconds)
print("totals", DELTA.total_seconds())
