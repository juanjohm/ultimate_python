"""Plantillas"""

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from pathlib import Path
from string import Template
import smtplib


# Plantilla HTML
# plantilla = """
#     <b>Hola Mundo $usuario</b>
# """
plantilla = Path("modulos-nativos/plantilla.html").read_text(encoding="UTF-8")
template = Template(plantilla)
#CUERPO = template.substitute({"usuario": "Juan"})
CUERPO = template.substitute(usuario="Jose")

# Adjunatr una imágen al MENSAJE
path = Path("12-modulos/imagen.png")
mime_image = MIMEImage(path.read_bytes())

# Construcción del MENSAJE
MENSAJE = MIMEMultipart()
MENSAJE["from"] = "Juan"
MENSAJE["to"] = "email@gmail.com"
MENSAJE["subject"] = "Prueba"
CUERPO = MIMEText(CUERPO, "html")

MENSAJE.attach(CUERPO)
MENSAJE.attach(mime_image)

# Enviar MENSAJE
with smtplib.SMTP(host="smtp.gmail.com", port=587) as smtp:
    smtp.ehlo()
    smtp.starttls()

    smtp.login("email@gmail.com", "contraseña")
    smtp.send_message(MENSAJE)
    print("Mensaje enviado")
