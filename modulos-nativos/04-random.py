"""Random"""

import random
import string

LISTA = [1, 2, 3, 4, 5, 6, 7, 8]
LISTA2 = [1, 2, 3, 4, 5, 6, 7, 8]
random.shuffle(LISTA)

# print(
#     random.random(),
#     random.randint(1, 10),
#     LISTA,
#     random.choice(LISTA2),
#     random.choices(LISTA2, k=3),
#     random.choices("abdjldfjkg", k=3),
#     "".join(random.choices("abdjldfjkg", k=3)),
#     )

CHARS = string.ascii_letters
DIGITOS = string.digits
SELECCION = random.choices(CHARS + DIGITOS, k=16)
# 
CONTRASENA = "".join(SELECCION)
print(CONTRASENA)
