"""Cli"""

from pathlib import Path
import sys
import os

def cli(args):
    if len(args) == 1:
      print("No se pasaron argumentos")
      return
    
    if len(args) != 3:
       print("Se necesitan 2 argumentos")
       return

    ORIGEN = args[1]
    O = Path(ORIGEN)
    if not O.exists():
        print("Origen no existe")
        return

    DESTINO = args[2]
    D = Path(DESTINO)
    if D.exists():
        print("El destino no puede existir")
        return

    os.rename(str(ORIGEN), str(DESTINO))

    print("Archivo renombrado con éxito")

cli(sys.argv)
