"""Email"""

# Habilitar envio de email  SMTP para gmail
# https://myaccount.google.com/u/1/lesssecureapps

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from pathlib import Path
import smtplib

#imagen
PATH = Path("modulos-nativos/imagen.png")
MIME_IMAGE = MIMEImage(PATH.read_bytes())

#Mensaje
MENSAJE = MIMEMultipart()
MENSAJE["from"] = "Juan"
MENSAJE["to"] = "juanjo2979@gmail.com"
MENSAJE["subject"] = "Prueba"
CUERPO = MIMEText("Cuerpo del mensaje")

MENSAJE.attach(CUERPO)
MENSAJE.attach(MIME_IMAGE)

MENSAJE.attach(CUERPO)

# Enviar mensaje
with smtplib.SMTP(host="smtp.gmail.com", port=587) as smtp:
    smtp.ehlo()
    smtp.starttls()

    smtp.login("email@gmail.com", "contraseña")
    smtp.send_message(MENSAJE)
    print("Mensaje enviado")
