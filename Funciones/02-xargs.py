"""Xargs"""

def suma(*NUMEROS):
    RESULTADO = 0
    for NUMERO in NUMEROS:
        RESULTADO += NUMERO
    print(RESULTADO)

suma(2, 5, 7)
suma(2, 5, )
suma(2, 8, 7, 45, 32)