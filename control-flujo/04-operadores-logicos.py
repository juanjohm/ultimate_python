"""Operadores Logicos"""

# and, or, not

GAS = False
ENCENDIDO = True
EDAD = 18

if GAS and ENCENDIDO:
    print("Avanzar")

print("---------------")

if GAS or ENCENDIDO:
    print("Avanzar")

print("---------------")

if not GAS or ENCENDIDO:
    print("Avanzar")

print("---------------")

if  GAS and ENCENDIDO and EDAD > 17:
    print("Avanzar")

print("---------------")

if  GAS and (ENCENDIDO or EDAD > 17):
    print("Avanzar")

print("------Operador cortocircuito---------")

if  not GAS and ENCENDIDO and EDAD > 17:
    print("Avanzar")
