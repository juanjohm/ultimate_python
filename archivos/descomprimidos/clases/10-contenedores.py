"""Contenedores"""

class Producto:
    def __init__(self, nombre, precio):
        self.nombre = nombre
        self.precio = precio

    def __str__(self):
        return f"Producto: {self.nombre} - Precio: {self.precio}"

class Categoria:
    productos = []

    def __init__(self, nombre, productos):
        self.nombre = nombre
        self.productos = productos

    def agregar(self, prodcuto):
        self.productos.append(prodcuto)
        
    def imprimir(self):
        for prodcuto in self.productos:
            print(prodcuto)

KAYAK = Producto("Kayak", 200)
BICI = Producto("Bici", 250)
PELOTA = Producto("Pelota", 50)
DEPORTES = Categoria("Deportes", [KAYAK, BICI])
DEPORTES.agregar(PELOTA)
DEPORTES.imprimir()
