"""Mágicos"""

class MiPerro:

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
    #destructor
    def __del__(self):
        print(f"Chao perro {self.nombre}")
    
    def __str__(self):
        return f"Clase Perro: {self.nombre}"

    def habla(self):
        print(f"{self.nombre} dice: Guauuauauau")


PERRO1 = MiPerro("toto", 7)
del PERRO1
