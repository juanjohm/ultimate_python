"""Herencia Multiple"""

class Caminador:
    def caminar(self):
        print("caminando")

class Volador:
    def volar(self):
        print("volando")

class Nadador:
    def nadar(self):
        print("nadando")

class Perro(Caminador, Nadador):
    def ladrar(self):
        print("Ladrando")

class Pato(Caminador, Nadador, Volador):
    def ladrar(self):
        print("Vive en el agua")
