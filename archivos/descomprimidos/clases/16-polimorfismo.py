"""Polimorfismo"""

from abc import ABC, abstractmethod


class Model(ABC):
    @abstractmethod
    def guardar(self):
        pass

class Usuario(Model):
    def guardar(self):
        print("Guardando en BBDD")

class Sesion(Model):
    def guardar(self):
        print("Guardando en archivo")


def guardar(entidades):
    for entidad in entidades:
        entidad.guardar()

USUARIO = Usuario()
SESION =Sesion()

guardar([SESION, USUARIO])#Polimorfismo
#guardar(USUARIO)
