"""Números"""

NUMERO = 2
DECIMAL = 1.2
IMAGINARIO = 2 +2j

# NUMERO = NUMERO + 2
# NUMERO += 2
# NUMERO -= 2
# NUMERO *= 2
# NUMERO /= 2

print("NUMERO", NUMERO)
print(1 + 3)
print(1 - 3)
print(1 * 3)
print(1 / 3)
print(1 // 3)
print(8 % 3)
print(2 ** 3)
