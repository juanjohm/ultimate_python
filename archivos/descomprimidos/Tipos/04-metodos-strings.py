"""Metodos"""
ANIMAL = "  perrO rojo  "
print(ANIMAL.upper())
print(ANIMAL.lower())
print(ANIMAL.strip().capitalize())
print(ANIMAL.title())
print(ANIMAL.strip())
print(ANIMAL.lstrip())
print(ANIMAL.rstrip())
print(ANIMAL.find("ro"))
print(ANIMAL.replace("rojo", "verde"))
print("rojo" in ANIMAL)
print("rojo" not in ANIMAL)
