"""Calculadora"""

NUMERO1 = input("Ingresa primer número")
NUMERO2 = input("Ingresa segundo número")

NUMERO1 = int(NUMERO1)
NUMERO2 = int(NUMERO2)

SUMA = NUMERO1 + NUMERO2
RESTA = NUMERO1 - NUMERO2
MULTI = NUMERO1 * NUMERO2
DIV = NUMERO1 / NUMERO2

MENSAJE = f"""
Para los números {NUMERO1} y {NUMERO2}, 
el resultado de la suma es {SUMA}
el resultado de la resta es {RESTA}
el resultado de la multiplicación es {MULTI}
el resultado de la división es {DIV}
"""
print(MENSAJE)
