"""IF"""

EDAD = 22
if EDAD > 17:
    print("Eres mayor de edad")

print ("Listo")

print("-------------------------")

EDAD = 15
if EDAD > 17:
    print("Eres mayor de edad")
else:
    print("No eres mayor de edad")

print ("Listo")

print("-------------------------")

EDAD = 55
if EDAD > 54:
    print("Puedes ver la película con descuento")
elif EDAD > 17:
    print("Eres mayor de edad")
else:
    print("No eres mayor de edad")

print ("Listo")

print("-------------------------")

EDAD = 66
if EDAD > 65:
    print("Puedes ver la película con descuento extra")
elif EDAD > 54:
    print("Puedes ver la película con descuento")
elif EDAD > 17:
    print("Eres mayor de edad")

print ("Listo")
