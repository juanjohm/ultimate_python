"""Calculadora"""

print("Bienvenidos a la calculadora")
print("Para salir escribe salir")
print("Las operaciones son suma, resta, multi y div")

RESULTADO = ""
while True:
    if not RESULTADO:
        RESULTADO = input("Ingrese número ")
        if RESULTADO.lower() == "salir":
            break
        RESULTADO = int(RESULTADO)
    OP = input("Ingresa operación: ")
    if OP.lower() == "salir":
        break
    NUM2 = input("Ingrese siguiente número: ")
    if NUM2.lower() == "salir":
        break
    NUM2 = int(NUM2)

    if OP.lower () == "suma":
        RESULTADO += NUM2
    elif OP.lower () == "resta":
        RESULTADO -= NUM2
    elif OP.lower () == "multi":
        RESULTADO *= NUM2
    elif OP.lower () == "div":
        RESULTADO /= NUM2
    else:
        print("Operación no valida")
    print(f"El resultado es {RESULTADO}")
