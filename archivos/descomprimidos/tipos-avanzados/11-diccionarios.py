"""Diccionarios"""

PUNTO = { "x": 25, "y": 50 }
print(PUNTO)
print(PUNTO["x"])
print(PUNTO["y"])

PUNTO["z"] = 45
print(PUNTO)

if "lala" in PUNTO:
    print("Esta")

print(PUNTO.get("x"))
print(PUNTO.get("lala"))
print(PUNTO.get("lala", 97))

del PUNTO["x"]
del(PUNTO["y"])
print(PUNTO)

PUNTO["x"] = 25
for valor in PUNTO:
    print(valor, PUNTO[valor])

for valor in PUNTO.items():
    print(valor)

for llave, valor in PUNTO.items():
    print(llave, valor)

USUARIOS = [
    {"id": 1, "nombre": "Juanjo"},
    {"id": 2, "nombre": "Pepe"},
    {"id": 3, "nombre": "Lola"},
    {"id": 4, "nombre": "Luis"}
]

for usuario in USUARIOS:
    print(usuario["nombre"])
