"""Deprador desempaquetado"""

LISTA = [1, 2, 3, 4]
print(LISTA)

print(*LISTA)

LISTA2 = [5, 6]

COMBINADA = ["Hola", *LISTA, "Mundo", *LISTA2]
print(COMBINADA)


PUNTO1 = {"X": 19, "Y": "HOLA"}
PUNTO2 = {"Y": 15}

NUEVO_PUNTO = {**PUNTO1, **PUNTO2, "Z": "MUNDO"}
print(NUEVO_PUNTO)
