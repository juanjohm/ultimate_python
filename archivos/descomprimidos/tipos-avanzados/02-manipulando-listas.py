"""Manipulando listas"""

MASCOTAS = ["Perro", "Gato", "Burro", "Tortuga"]

print(MASCOTAS[0])

MASCOTAS[0] = "Mariposa"

# print(MASCOTAS[2:])
# print(MASCOTAS[:])
# print(MASCOTAS[0:3])
# print(MASCOTAS[:3])
# print(MASCOTAS[::2])
print(MASCOTAS[1:2:2])

NUMEROS = list(range(1, 21))
print(NUMEROS)
print(NUMEROS[::2])
print(NUMEROS[::2])
