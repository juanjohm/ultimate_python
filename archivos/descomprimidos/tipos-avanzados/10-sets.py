"""Sets"""

#Set son grupos o conjuntos, elimina los duplicado automaticamente

# PRIMER = {1, 1, 2, 2, 3, 4}
# PRIMER.add(5)
# PRIMER.remove(1)
# print(PRIMER)

PRIMER = {1, 1, 2, 2, 3, 4}
SEGUNDO = [3, 4, 5]
SEGUNDO = set(SEGUNDO)

print(SEGUNDO)
print(PRIMER | SEGUNDO)
#devuelve los elementos que esten en los 2 set
print(PRIMER & SEGUNDO)
#devuelve 1 y 2 que son los que no estan en el set SEGUNDO
print(PRIMER - SEGUNDO)
#devuelve los elemnetos no duplicados en ambos set o elimina los duplicados
print(PRIMER ^ SEGUNDO)
