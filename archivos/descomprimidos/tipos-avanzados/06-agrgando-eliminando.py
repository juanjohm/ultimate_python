"""Agregar y elinar"""

MASCOTAS = [
    "laura", 
    "perro",
    "gato",
    "gato", 
    "tortuga", 
    "miguel",
    "pajaro", 
    "gato"
]
#insertar en posicion dada
MASCOTAS.insert(1, "jose")
#in sertar al final
MASCOTAS.append("Luis")

#elimina la primera que encuentre
MASCOTAS.remove("gato")
#elimina último elemento
MASCOTAS.pop()
#elimina  elemento dado
MASCOTAS.pop(1)
#elimina elemento dado
del MASCOTAS[0]
#elimina todo
MASCOTAS.clear()

print(MASCOTAS)
