"""Filas"""

from collections import deque

FILA = deque([1, 2])
# FILA.append(3)
# FILA.append(4)
# FILA.append(5)

print(FILA)
FILA.popleft()
FILA.popleft()
print(FILA)
if not FILA:
    print("Fila vacia")
