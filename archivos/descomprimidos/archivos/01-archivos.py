"""Archivos"""

from pathlib import Path
from time import ctime

ARCHIVO = Path("archivos/prueba.txt")

# ARCHIVO.exists()
# ARCHIVO.rename()
# ARCHIVO.unlink()

print("acceso", ctime(ARCHIVO.stat().st_atime))
print("creación", ctime(ARCHIVO.stat().st_ctime))
print("modificación", ctime(ARCHIVO.stat().st_mtime))
