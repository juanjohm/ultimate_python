"""Json"""

import json
from pathlib import Path

#escribir json
# PRODUCTOS = [
#     {"id": 1, "name": "Surf"},
#     {"id": 2, "name": "bici"},
#     {"id": 3, "name": "skate"}
# ]

# DATA = json.dumps(PRODUCTOS)
# #print(DATA)

# Path("archivos/productos.json").write_text(DATA)

# leer
DATA = Path("archivos/productos.json").read_text(encoding="utf-8")
PRODUCTOS = json.loads(DATA)
#print(PRODUCTOS)

# modificar json
PRODUCTOS[0]["name"] = "bici2"
Path("archivos/productos.json").write_text(json.dumps(PRODUCTOS))
