"""Path"""

from pathlib import Path

# Path(r"C:\Program Files\AutoFirma")#windows
# Path(r"C:/usr/bin")#Linux
# Path.home()#personal
# Path("one/__init__.py")#archivo

PATH = Path("Hola-mundo/mi-archivo.py")
PATH.is_file()
PATH.is_dir()
PATH.exists()

print(
    PATH.name,
    PATH.stem,
    PATH.suffix,
    PATH.parent,
    PATH.absolute()
)

P = PATH.with_name("archivo.py")
print(P)
P = PATH.with_suffix(".bat")
print(P)
P = PATH.with_stem("roto")
print(P)
