"""Open"""

from io import open

#escritura
# TEXTO = "Hola Mundo"

# ARCHIVO = open("archivos/Hola.txt", "w")
# ARCHIVO.write(TEXTO)
# ARCHIVO.close()

#lectura
# ARCHIVO = open("archivos/Hola.txt", "r")
# TEXTO = ARCHIVO.read()
# ARCHIVO.close()
# print(TEXTO)

#lectura como lista
# ARCHIVO = open("archivos/Hola.txt", "r")
# TEXTO = ARCHIVO.readlines()
# ARCHIVO.close()
# print(TEXTO)

#with y seek
# with open("archivos/Hola.txt", "r") as ARCHIVO:
#     print(ARCHIVO.readlines())
#     ARCHIVO.seek(0)
#     for linea in ARCHIVO:
#         print(linea)

#agragar
# ARCHIVO = open("archivos/Hola.txt", "a+")
# ARCHIVO.write(" chao mundo :(")
# ARCHIVO.close()

#Lectura y escritura
with open("archivos/Hola.txt", "r+") as ARCHIVO:
    TEXTO = ARCHIVO.readlines()
    ARCHIVO.seek(0)
    TEXTO[0] = "Modifico primera lineaaaaaaaaaa"
    ARCHIVO.writelines(TEXTO)
