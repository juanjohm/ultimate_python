from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from .forms import ProductoForm
from .models import Producto
# Create your views here.

# /productos
def index(request):
    PRODUCTOS = Producto.objects.all()

    return render(
        request,
        'productos.html',
        context={'PRODUCTOS': PRODUCTOS}
    )

def detalle(request, producto_id):
    ### Este codigo se puede sustituir por el no comnetado ###
    # try:
    #     PRODUCTO = Producto.objects.get(id=producto_id)

    #     return render(
    #         request, 
    #         'detalle.html', 
    #         context={ 'PRODUCTO': PRODUCTO }
    #         )
    # except Producto.DoesNotExist:
    #     raise Http404()
    PRODUCTO = get_object_or_404(Producto, id=producto_id)

    return render(
        request,
        'detalle.html',
        context={'PRODUCTO': PRODUCTO}
    )

def formulario(request):
    if request.method == 'POST':
        form = ProductoForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/productos')
    else:
        form = ProductoForm()

    return render(
        request,
        'producto_form.html',
        {'form': form}
    )
