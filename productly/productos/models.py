from django.db import models
from django.utils import timezone

# Create your models here.
class Categoria(models.Model):
    NOMBRE = models.CharField(max_length=255)

    def __str__(self):
        return self.NOMBRE

class Producto(models.Model):
    NOMBRE = models.CharField(max_length=255)
    STOCK = models.IntegerField()
    PUNTUACION = models.FloatField()
    ####  on_delete  #####
    # CASCADE: eliminar el producto
    # PROTECT: lanza un error
    # RESTRICT: elimina si no esxisten productos
    # SET_NULL: actualiza a valor nulo
    # SET_DEFAULT: asigna valor por defecto 
    CATEGORIA = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    FECHA_CREACION = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return self.NOMBRE
