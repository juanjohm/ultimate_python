from django.contrib import admin
from .models import Categoria, Producto

class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('id', 'NOMBRE')

class ProductoAdmin(admin.ModelAdmin):
    exclude = ('FECHA_CREACION', )
    list_display = ('id', 'NOMBRE', 'STOCK', 'FECHA_CREACION')

# Register your models here.
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Producto, ProductoAdmin)
