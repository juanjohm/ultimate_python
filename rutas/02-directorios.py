"""Directorios"""

from pathlib import Path

PATH = Path("rutas")
# PATH.exists()
# PATH.mkdir()
# PATH.rmdir()
# PATH.rename("rename")

# for p in PATH.iterdir():
#     print(p)

ARCHIVOS = [p for p in PATH.iterdir() if not p.is_dir()]
print(ARCHIVOS)
ARCHIVOS = [p for p in PATH.glob("*.py")]
print(ARCHIVOS)
ARCHIVOS = [p for p in PATH.glob("01-*.py")]
print(ARCHIVOS)
ARCHIVOS = [p for p in PATH.glob("**/*.py")]
print(ARCHIVOS)
ARCHIVOS = [p for p in PATH.rglob("*.py")]
print(ARCHIVOS)
